package com.example.tpecf;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tpecf.adapter.ProductAdapter;
import com.example.tpecf.databinding.ProductsListBinding;
import com.example.tpecf.model.Product;
import com.example.tpecf.util.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListProductFragment extends Fragment {

private ProductsListBinding binding;

private RecyclerView products_view;

private ProductAdapter adapter;

    public ListProductFragment(){

    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

      binding = ProductsListBinding.inflate(inflater, container, false);
      return binding.getRoot();


    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ProductAdapter(new ProductAdapter.ProductDiff(), ListProductFragment.this);
        binding.productsView.setAdapter(adapter);
        binding.productsView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.productsView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        RetrofitClient.getInstance().getApiService().getProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                List<Product> products = response.body();

                adapter.submitList(products);
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toast.makeText(getContext(), "erreur", Toast.LENGTH_LONG).show();
                System.out.println(t.getMessage());
            }
        });

    }

@Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}