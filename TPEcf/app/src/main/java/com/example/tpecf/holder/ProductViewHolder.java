package com.example.tpecf.holder;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tpecf.R;
import com.example.tpecf.model.Product;
import com.example.tpecf.service.ProductApiService;

public class ProductViewHolder extends RecyclerView.ViewHolder {

    private TextView titleTextView;

    private Button button;

    private Fragment _fragment;

    private TextView priceTextView;

    private TextView descriptionTextView;

    private TextView catgeroyTextView;

    private ImageView imageView;

    private TextView ratingTextView;

    private TextView countTextView;

    private ProductApiService productApiService;


    public ProductViewHolder(@NonNull View itemView, Fragment fragment){
        this(itemView);
        _fragment = fragment;
    }

    public ProductViewHolder(@NonNull View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.titleTextView);

    }

    public void display(Product product, Runnable method) {
        titleTextView.setText(product.getTitle());
        titleTextView.setOnClickListener(new AdapterView.OnClickListener() {
    
            @Override
            public void onClick(View v) {
                ListContactFragmentDirections.ActionListToDetail action = ListContactFragmentDirections.actionListToDetail(i);
                NavHostFragment.findNavController(ListContactFragment.this).navigate(action);
            }
        });
    }

    public static ProductViewHolder create(ViewGroup parent, Fragment fragment) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ProductViewHolder(view, fragment);
    }


}
